import numpy as np
import matplotlib.pyplot as plt
import sys

def powerPlot(filename='powers.txt'):
#    E, Mod1In, Mod1Out, Mod2In, Mod2Out, J, chi2 = np.loadtxt(filename, unpack=True)
    E,Mod1Out, Mod2Out, ModTHz, Current = np.loadtxt(filename,usecols=(0,2,4,7,5), unpack=True)
    ModTHz = abs(ModTHz) * 500
    ModTHz[0:7] = 0
    ETHz = list(E)
    ETHz.insert(8,41.2); ModTHz = list(ModTHz); ModTHz.insert(7,3); ModTHz[-1]=0;
    plt.figure()
    plt.plot(E, Mod1Out, 'b-', linewidth=2, label='30.906THz')
    plt.plot(E, Mod2Out, 'r-', linewidth=2, label='28.018THz')
    plt.plot(ETHz, ModTHz, 'g--o', linewidth=2, label='2.888THz')
    plt.xlabel("E [kV/cm]")
    plt.ylabel("P [mW]")
    plt.legend()
    plt.figure()
    plt.plot(E, Current,'-o',linewidth=2)
    plt.title('Current')
    plt.xlabel("E [kV/cm]")
    plt.ylabel("J [$kA/cm^2$]")
    plt.show(block=False)

def simplePowerPlot(filename='powers_withNonParabolicities.txt'):
    E,Mod1, Mod2, Current = np.loadtxt(filename,usecols=(0,3,4,5), unpack=True)
    plt.figure()
    plt.plot(E, Mod1, 'b*-', linewidth=2, label='30.906THz')
    plt.plot(E, Mod2, 'ro-', linewidth=2, label='28.018THz')
    plt.xlabel("Bias [kV/cm]")
    plt.ylabel("MIR power [mW]")

    ax1 = plt.gca()
    ax2 = ax1.twinx()
    ax2.plot(E, Current, 'g--')
    for tl in ax2.get_yticklabels():
            tl.set_color('g')
    ax2.set_ylabel(r'Current density [$kA/cm^2$]', color='g')
    plt.show(block=True)

if(__name__=='__main__'):
    """
    Plot outcoupled modes for "Files" given as arguments in the command line. 
    The files should be formatted in columns, using spaces as separators 
    (no commas). The columns must be arranged as: 
    E[kVcm]  Mod1_In  Mod2_In  Mod1_Out  Mod2_Out  Current[kA/cm^2] 

    "In", "Out" denote the fields inside and outside the QCL respectively. 
    The modes are assumed to be in units of "mW". If there are more columns in the 
    "Files" these are simply discarded and no problem arises.

    If no "File" is introduced from the command line, the program requests it.
    """
    def getFiles():
        """
        Collects arguments (filenames) introduced in command line and returns
        them as a tuple of strings. If no argument is introduces it requests
        it.
        """
        #No argument introduced
        if(len(sys.argv)==1):
            File = input("Enter Input File: ")
            return (File,)
        #Inputfile(s) was provided
        elif(len(sys.argv)>=2):
            return tuple(sys.argv[1:]) 

    Files = getFiles()
    n = len(Files)
    for i in range(n):
#        print(Files[i])
        simplePowerPlot(Files[i])


