import numpy as np
import matplotlib.pyplot as plt

def powerPlot(filename='powers_higherLosses.txt'):
#    E, Mod1In, Mod1Out, Mod2In, Mod2Out, J, chi2 = np.loadtxt(filename, unpack=True)
    E,Mod1Out, Mod2Out, ModTHz, Current = np.loadtxt(filename,usecols=(0,2,4,7,5), unpack=True)
    ModTHz[0:3] = 0
    k=140
    ModTHz = abs(ModTHz) * k
    ETHz = list(E); ModTHz = list(ModTHz);
#    ETHz.insert(3,41.2);  ModTHz.insert(3,1*k); 
#    plt.figure()
    plt.plot(E, Mod1Out, 'b*-', linewidth=2, label='30.906THz')
    plt.plot(E, Mod2Out, 'ro-', linewidth=2, label='28.018THz')
    plt.plot(ETHz, ModTHz, 'g--o', linewidth=2, label='2.888THz')
    plt.xlabel("E [kV/cm]")
    plt.ylabel("P [mW]")
    plt.legend()
    plt.figure()
    plt.plot(E, Current,'-o',linewidth=2)
    plt.title('Current')
    plt.xlabel("E [kV/cm]")
    plt.ylabel("J [$kA/cm^2$]")
    plt.show(block=False)

#if(__name__=='__main__'):
#	powerPlot()

