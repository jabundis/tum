#Dual Upper State Belkin
#Mode1 = 30.906THz  (blue)
#Mode2 = 28.018THz  (red)
#R = 0.290
#With NonParabolicities (beta) and 12 wavefunctions
#EMC with improved tunnelling implementation (EMC_May2018)
# cav.dat:
# 3.248  2  0
# 30.906e12  1252.8  0.783
# 28.018e12  1198.4  0.749

#E                  Power(mW)             Current            chi2                 Power THz radiation
#[kV/cm]     In(avg)         Out(avg)    (kA/cm^2)          (m/V)                   (Adimensional) 
#         Mod1    Mod2    Mod1    Mod2                                           oldTHz_ RI   newTHZ_RI 
35           0    1111       0     612     4.987     -1.5037e-08+1.1398e-08i
37          65     147      36      81     4.276     -1.3870e-08-4.2948e-09i
38         995       9     548       5     4.679     -1.1413e-08-4.4300e-09i  
40        1330       0     732       0     4.78      -5.9014e-09+4.3411e-09i
42        1160       1     638       0     4.539     -1.6982e-09+1.5387e-08i
44          19     620      11     341     4.056     -2.7707e-09+2.2089e-08i
