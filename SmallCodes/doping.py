def sheetDoping(L, dopingDist, doping):
    """
    Calculates sheet doping density

    input:
        L --> Iterable with thicknesses of the structure in Angstrom
        dopingDist --> Total length of doped layers in active medium [Angs]
        doping --> Doping density in active medium [cm-3]

    output:
        prints "average doping density" and "sheet doping density"
        returns None
    """
    Lp = sum(L)
    avgDonori = (dopingDist/Lp) * doping
    sheetDoping = (avgDonori * (Lp/1e8)) * 1e4
    print("Avg donori: {:.3e} A-3".format(avgDonori * 1e-24))
    print("sheet Doping: {:.3e} m-2".format(sheetDoping))
#    return (avgDonori, sheetDoping)

def percentEvenElements(L):
    res = 0
    for i, elem in enumerate(L):
            if((i+1)%2==0): res += L[i]
#    return (res, res/sum(L))
    return res/sum(L)

#Faist structure
L_Faist = [40, 18, 8, 53, 10, 48, 11, 43, 14, 36, 17, 33, 24, 31, 34, 29]

#Belkin Structure
L_Belkin = [38, 38, 23, 85, 10, 69, 11, 56, 12, 48, 13, 45, 14, 42, 16, 41, 18, 40, 23, 40, 26, 40]

#Green Structure
L_Green = [35, 90, 6, 163, 9, 160, 10, 138, 12, 120, 15, 110, 24, 110, 32, 121]

#St_Jean Structure
L_Jean = [40, 19, 7, 58, 9, 57, 9, 50, 22, 34, 14, 33, 13, 32, 15, 31, 19, 30, 23, 29, 25, 29]

#Fujita 2017
L_Fujita = [37, 24, 26, 60, 9, 49, 11, 45, 12, 36, 15, 32, 16, 30, 18, 29, 21, 28, 24, 27, 28, 26]
