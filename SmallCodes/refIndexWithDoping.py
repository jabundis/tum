import numpy as np

def epsFromRefInd(nList):
    """
    Returns dielectric constants for the complex refractive indices
    in "nList". (It's assumed a relative permeability of one
                 and planewave of form exp(-iwt), i.e, eps =
                 eps1 + i*eps2)

    Input:
        nList (np.ndarray or list) --> refractive indices
            nList[i]->float or complex

    Returns:
        eps (np.ndarray) --> complex dielectric constants
    """
    nList = np.array(nList)
    n = np.real(nList); k = np.imag(nList)
    eps = (n**2-k**2) + 1j*(2*n*k)
    return eps

def epsilon(w, eps_core, ne, tau, effMass):
    """
    returns a np.array with the dielectric constants accounting for
    free-carrier losses by using the Drude-Lorentz approximation
    for a np.ndarray of circular frequencies "w".

    Input:
        w (np.array) --> circular frequencies [rad/sec]
        eps_core (np.array or a constant) --> undoped dielectric constants
                                              for w. Elements are floats
                                              or complex numbers.
        ne --> free carrier density [cm-3]
        tau --> electron relaxation time [ps]
        effMass --> electron effective mass [SI units]

    Returns:
        eps (np.array) --> complex dielectric constants [adimensional]
    """
    from fundamentalConstants import e, eps0
    w = np.array(w)

    #conversion to SI units
    ne = ne * 1e6
    tau = tau * 1e-12

    eps = eps_core + (1j*ne * e**2 * tau)/(w * effMass * (1-1j*w*tau))/eps0
    return eps

def refIndex(eps):
    """
    Returns the complex refractive indices for dielectric constants
    given in the np.array "eps". It consideres a relative magnetic
    permeability equal to one.

    Input:
        eps (np.array) --> complex dielectric constants

    Returns:
        indices (2d np.array) --> complex refractive indices
            where:
                1st column: real parts.
                2nd column: imaginary parts
    """
    n = np.sqrt( (np.absolute(eps) + np.real(eps))/2 )
    k = np.imag(eps) / (2*n)
    indices = np.zeros((len(eps),2))
    indices[:,0] = n; indices[:,1] = k
    return indices

def absorptConst(w, eps):
    """
    Returns the absorption coefficients for the circular frequencies
    "w" and corresponding complex dielectric constants "eps".

    Input:
        w (np.ndarray or list) --> circular frequencies [rad/s]
        eps (np.ndarray, type complex) --> dielectric constants

    Returns:
        absorption (np.ndarray, type float) --> absorption constants
                                                    [cm-1]
    """
    from fundamentalConstants import c
    w = np.array(w)
    refInd = refIndex(eps)
    k = refInd[:,1]  #Imaginary part Ref. Index
    absorption = 2* w * k / c
    return absorption * 1e-2

class Material:
    def __init__(self, n, effMass):
        self.n = n #refractive index at frequency of interest
        self.effMass = effMass  #electron effective mass at band edge [Kg]
    def set_effMass(self, newMass):
        self.effMass = newMass
    def set_n(self, newN):
        self.n = newN
    def er_core(self, f):
        """
        Returns np.ndarray with the undoped complex dielectric
        constants for linear frequencies in np.ndarray or list "f"
        """
        eps_core = epsFromRefInd([self.n])[0]
        eps_core = np.ones(len(f)) * eps_core
        return eps_core
    def refracIndex(self, f, ne, tau):
        eps_core = self.er_core(f)
        f = np.array(f)
        w = 2 * np.pi * f
        eps = epsilon(w, eps_core, ne, tau, self.effMass)
        return refIndex(eps)
    def absorption(self, f, ne, tau):
        """
        Returns absorption coefficients for linear frequencies in
        np.ndarray or list "f".
        Input:
            f (np.ndarray or list) --> linear frequencies [Hz]
            ne --> free carrier density [cm-3]
            tau --> electron relaxation time [ps]

        Returns:
            absorption (np.ndarray, type float) --> absorption constants
        """
        eps_core = self.er_core(f)
        f = np.array(f)
        w = 2 * np.pi * f
        eps = epsilon(w, eps_core, ne, tau, self.effMass)
        return absorptConst(w, eps)

class InP(Material):
    """
    Returns basic parameters of "InP"
    """
    from fundamentalConstants import me
    def __init__(self, n=3.63, effMass=0.073*me):
        self.n = n #refractive index without doping at 180cm-1
        self.effMass = effMass  #electron effective mass at band edge [SI]
#    def set_effMass(self, newMass):
#        self.effMass = newMass
#    def set_n(self, newN):
#        self.n = newN
#    def er_core(self, f):
#        """
#        Returns np.ndarray with the undoped complex dielectric
#        constants for linear frequencies in np.ndarray or list "f"
#        """
#        eps_core = epsFromRefInd([self.n])[0]
#        eps_core = np.ones(len(f)) * eps_core
#        return eps_core

#    def absorption(self, f, ne, tau):
#        """
#        Returns absorption coefficients for linear frequencies in
#        np.ndarray or list "f".
#        Input:
#            f (np.ndarray or list) --> linear frequencies [Hz]
#            ne --> free carrier density [cm-3]
#            tau --> electron relaxation time [ps]
#
#        Returns:
#            absorption (np.ndarray, type float) --> absorption constants
#        """
#        eps_core = self.er_core(f)
#        f = np.array(f)
#        w = 2 * np.pi * f
#        eps = epsilon(w, eps_core, ne, tau, self.effMass)
#        return absorptConst(w, eps)
