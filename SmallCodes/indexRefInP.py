import numpy as np
import fundamentalConstants as FC
import sys
"""
Calculates the refractive index for InP for frequencies in
in the range:

If run as a script, introduce as a command argument the 
frequency in hertz at which to calculate the refractive index. 
If not introduced f=30Thz is used by default
"""
def getFreq():
    #Default frequency
    if(len(sys.argv)==1):
        return 30e12   #[hertz]
    #Introduced frequency
    elif(len(sys.argv)==2):
        return float(sys.argv[1]) #[hertz]
    else:
        raise Exception("Must introduce only one parameter")

def getRefractiveIndex(freq): 
    """
    Returns refractive indices for frequencies "freq" in "hertz"
    """
    #retrieve energy E
    freq = np.array(freq)
    E = FC.hev * (2*np.pi * freq) #[eV]

    #model constants
    E0 = 1.345; E1 = 3.2; E2 = 5.1; E3 = 37.65e-3  #all in "eV" 
    G1 = 57.889; G2 = 65.937; G3 = 0.392e-2 #all in "eV^2" 
    A = 0.7 * np.sqrt(E0)

    #Ref. index squared
    n2 = 1 + A/np.pi*np.log( (E1**2 - E**2)/(E0**2 - E**2) ) + \
        G1/(E1**2 - E**2) + G2/(E2**2 - E**2) + G3/(E3**2 - E**2)

    return np.sqrt(n2)

if (__name__=="__main__"):
    freq = getFreq()
    n = getRefractiveIndex(freq)
    print("frequency: {:.3e}".format(freq))
    print("n = {:.3f}".format(n))
