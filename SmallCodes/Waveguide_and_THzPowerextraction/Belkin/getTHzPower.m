function [Eb_n, ce, PTHz] = getTHzPower();

[x, Ex_f1, Hy_f1, Ex_f2, Hy_f2] = fields();
[w1,w2,eps,n,beta1,beta2, p1, p2, chi2] = parameters();
m = length(p1);
Eb_n = zeros(1,m); ce = zeros(1,m); PTHz = zeros(1,m);
for i=1:m
[Eb_n(i), ce(i), PTHz(i)] = nlwaveguide1(w1, w2, n, eps, chi2(i,:), Ex_f1, Ex_f2, x, beta1, beta2, p1(i), p2(i));
end

end
