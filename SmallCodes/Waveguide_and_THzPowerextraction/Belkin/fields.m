function [x, Ex_f1, Hy_f1, Ex_f2, Hy_f2] = fields();

function [x, Ex, Hy] = Ex_Hy(f)
    sol = eim7_myRefIndices(f);
    x = sol(1,:);
    Ex = sol(2,:);
    Hy = sol(3,:);
    IEx = abs(Ex).^2;
    IHy = abs(Hy).^2;
    xGain = [x(499*3 + 1), x(499*4 + 1)];

    figure
    plot(x, IEx, 'b-',[xGain(1),xGain(1)],[0,max(IEx)],'r--',[xGain(2),xGain(2)],[0,max(IEx)],'r--')
    title('|Ex|^2')
    xlabel('x [m]')
    legend(['f = ' num2str(round(f,3))])

    figure
    plot(x, IHy, 'b-',[xGain(1),xGain(1)],[0,max(IHy)],'r--',[xGain(2),xGain(2)],[0,max(IHy)],'r--')
    title('|Hy|^2')
    xlabel('x [m]')
    legend(['f = ' num2str(round(f,3))])
end

f = [30.906e12, 28.018e12];
[x, Ex_f1, Hy_f1] = Ex_Hy(f(1));
[x, Ex_f2, Hy_f2] = Ex_Hy(f(2));
end
