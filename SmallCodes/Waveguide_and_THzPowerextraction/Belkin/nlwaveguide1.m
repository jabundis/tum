function [Eb_n,ce,PTHz]=nlwaveguide1(w1,w2,n,eps,chi2,Ew1z1,Ew2z1,x,beta1,beta2,PEz1,PEz2); % input: w1,w2,n,eps,Ew1,Ew2,x,beta1,beta2,gridpoints

% comments refer partially to the equations used from Hashizume et al .
% �?Optical harmonic generation in multilayered structures : a comprehensive 
% analysis�?. The same equation numbers used in the paper are used in
% the comments .

eps0=8.8541878176e-12; 
c=3e8;
nl =9;   %number of layers (including substrate and upper contact)
gridpoints =500;


ns=n(1);

% calculate general constants and grid matrix
w3=w1-w2;
beta=(beta1-beta2);
k=(w3/c).*n; % n is the vector with THz refractive indices of all layers including substrate and cover
kappa=sqrt(k.^2-beta^2) ; % kappa is the wavevector component normal to the layers (z−component) 
for i=1:1:nl-1
    r(i)=(kappa(i+1)*eps(i)-kappa(i)*eps(i+1))/(kappa(i+1)*eps(i)+kappa(i)*eps(i+1));
    t(i)=(2*n(i+1)*n(i)*kappa(i+1))/(kappa(i+1)*eps(i)+kappa(i)*eps(i+1));
end

kk=0;
for i=1:1:nl-2
    z(:,i)=x(kk*(gridpoints-1)+1:kk*(gridpoints-1)+gridpoints); %each Col. is for a different layer
    Ew1z(:,i)=Ew1z1(kk*(gridpoints-1)+1:kk*(gridpoints-1)+gridpoints); % first fundamental field 
    Ew2z(:,i)=Ew2z1(kk*(gridpoints-1)+1:kk*(gridpoints-1)+gridpoints); % second fundamental field
    kk=kk+1;
end

m=size(z); % m = [gridpoints  nl-2]

% calculate nonlinear polarization for DFG process (at w3=w2−w1) using fundamental fields
% TM−Mode Ex, Ez, Hy => Py=0 % TE−Mode Ey, Hx, Hz (not relevant for QCLs) 
chi2m=repmat(chi2,m(1),1);
Px=zeros(m(1),m(2)); % negligible longitudinal Ex component 
Pz=eps0 .* abs(chi2m) .* (Ew1z) .* conj(Ew2z);
Py= zeros(m(1),m(2));
% calculate E self=E r+E b for all layers using equations 3.17a and 3.17b (kappa_i=k_iˆ2 betta3ˆ2 with k_i = (w3/c)∗n_i(w3) and betta3=|betta1−betta2| ) 
kappa=kappa(2:end-1);
k=k(2:end-1);
n=n(2:end-1);
eps=eps(2:end-1);

for i =1:1:nl-2 % nl is number of layers including infinite substrate and cover / nl−2 is number of layers without substrate and cover
    for j =1:1:gridpoints % gridpoints is number of grid points in each layer
        Erp_p(j,i)=(1i*w3^2/2/eps0/c^2/kappa(i))*trapz(z(1:j,i),(-(beta/k(i))*Pz(1:j,i)+(kappa(i)/k(i))*Px(1:j,i)) .* exp(-1i*kappa(i).*z(1:j,i)),1)*exp(1i*kappa(i)*z(j,i));
        Erp_n(j,i)=(1i*w3^2/2/eps0/c^2/kappa(i))*...
        trapz(z(j:gridpoints,i),(-(beta/k(i))*Pz(j:gridpoints,i)-(kappa(i)/k(i))*Px(j:gridpoints,i)).*exp(1i*kappa(i).*z(j:gridpoints,i)),1)*exp(-1i*kappa(i)*z(j,i));
    end
    Eb_z(:,i)=-1/(eps0*eps(i)).*Pz(:,i);
    Erp_z(:,i)=-beta/k(i).*(Erp_p(:,i)+Erp_n(:,i)); 
    Erp_x(:,i)=kappa(i)/k(i).*(Erp_p(:,i)-Erp_n(:,i));
end
% calculate E add i for each layer using transfer matrix method
% calculate for each layer M i and Phi i matrices using equations 4.4 and 4.6
T=eye(2,2);
M(:,:,1)=1/t(1).*[1 r(1) ; r(1) 1];
T(:,:,1)=M(:,:,1); 
for i=1:1:nl-2
    M(:,:,i+1)=1/t(i+1).*[1 r(i+1) ; r(i+1) 1];
    Phi(:,:,i)= [exp(1i*kappa(i)*(z(end,i)-z(1,i))) 0 ; 0 exp(-1i*kappa(i)*(z(end,i)-z(1,i))) ] ;
    T(:,:,i+1)=M(:,:,i+1)*Phi(:,:,i)*T(:,:,i); % accumulates transfer matrix; T(:,:, nl−1) gives overall transfer matrix from left to right T_RL − partially repeated by line 85
end

for i=1:1:nl-2
    % calculate source terms S_i for all layers using equation 4.9
    Sp(:,:,i)=[Erp_p(end,i); Erp_n(end,i)]-Phi(:,:,i)*[Erp_p(1,i); Erp_n(1,i)]; %p−polarization
end

% calculate T_ij (4.11a) and T_RL and S_RL/S_LR using 4.12b/4.13b 

h=1; 
Ta_Ss(:,:,1)=zeros(2,1); 
for i=2:1:nl-1
    for j=i-1:-1:1
        To=M(:,:,j+1) ;
        for kk=j:1:i-2
            To=M(:,:,kk+2)*Phi(:,:,kk+1)*To; % accumulates transfer matrix for field calculation
        end
        Ta_S(:,:,h)=To*Sp(:,:,j); % calculate T∗S for equation 4.16a 
        h=h+1;
    end
    Ta_Ss(:,:,i)=sum(Ta_S,3); % calculate summations of T∗S for equation 4.16a h=1;
    h=1;
end 
T_RL=T(:,:,end);

ErRs=[0;0]; ErLs=[0;0];

for i=1:1:nl-2
    S_iL(:,:,i)=T(:,:,i)*ErLs-[Erp_p(1,i); Erp_n(1,i)]+Ta_Ss(:,:,i); % T(:,:,i)=T_iL
end
S_RL=T(:,:,end)*ErLs-[ErRs(1,1); ErRs(2,1)]+Ta_Ss(:,:,end);

% calculate boundary values of E w3 by inserting 4.14a and 4.14b in 4.12a or 4.13a

Eb_p=(S_RL(1,1)-T(1,2,end)/T(2,2,end)*S_RL(2,1));
Eb_n=-1/T(2,2,end)*S_RL(2,1);

for i=1:1:nl-2 % nl is number of layers including infinite substrate and cover 
    for j=1:1:gridpoints % gridpoints is number of grid points in each layer
        Phi_d=[exp(1i*kappa(i)*(z(j,i)-z(1,i))) 0 ; 0 exp(-1i*kappa(i)*(z(j,i)-z(1,i)))];
        E_add=Phi_d*(T(:,:,i)*[0;Eb_n]+S_iL(:,:,i)); 
        E_add_p(j,i)=E_add(1,1);
        E_add_n(j,i)=E_add(2,1);
    end
    E_add_z(:,i)=-beta/k(i).*(E_add_p(:,i)+E_add_n(:,i));
    E_add_x(:,i)=kappa(i)/k(i).*(E_add_p(:,i)-E_add_n(:,i));
end

% sum E_add and E_self (E_r+E_b) to obtain total harmonic field

E_self_x=Erp_x;
E_self_z=Erp_z+Eb_z;
E_all_x=E_add_x+E_self_x;
E_all_z=E_add_z+E_self_z;

ex=size(E_all_x);

% convert matrices to vectors
E_x=reshape(E_all_x, ex(1)*ex(2), 1);
E_z=reshape(E_all_z, ex(1)*ex(2), 1);

% calculate Hy
for i=1:1:nl-2
    Hy(:,i)=(-w3/beta).*(eps0*eps(i)*E_all_z(:,i)+Pz(:,i)); 
end

% plot results
H_y=reshape(Hy, ex(1)*ex(2), 1);

% calculate conversion efficiency
ITHz=0.5*real((E_x(1))*conj(H_y(1))); 
%PTHz=ITHz*1.7*10^-3*22*10^-6;  % Calculate for your particular area
PTHz=ITHz*3.0*10^-3*12*10^-6;  % Calculate for your particular area
ce=PTHz/(PEz1*PEz2); 

end
