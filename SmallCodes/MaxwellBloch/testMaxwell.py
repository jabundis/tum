#Test Suite for twoLevels.py
import sys
sys.path.append('/home/jesus/Documents/tum/SmallCodes')

import Maxwell as MAX
import fundamentalConstants as FC
import numpy as np
import matplotlib.pyplot as plt


def testParams():
    length = 2.0e-4 # m
    roundtrips = 10
    M = 500
    f = 25e12  # s^(-1) 

    w =  2*np.pi*f
    N, deltaX, deltaT, simTime, timeRoundtrip = times(length, roundtrips, M)

    # Since we require c*deltaT < deltaX for convergence in cases where k<=0,
    # times are corrected (deltaT is reduced by 10%) 
    deltaT = 0.9*deltaT 
#    deltaT = 1.3e-15
    N = int(simTime / deltaT) 
    simTime = N * deltaT
    #roundtrips should also be corrected and is not neccessarily an integer anymore
    roundtrips = simTime/timeRoundtrip

    return (M, N, length, simTime, deltaX, deltaT, w, timeRoundtrip, roundtrips)

def times(length, roundtrips, M, n=1, c=FC.c):
    """
    M (int) -> Number of intervals in half a roundtrip
    """
    v = c/n
    timeRoundtrip = (2*length) / v
    simTime = roundtrips * timeRoundtrip
    N = (2*M) * roundtrips
    deltaX = length / M
    deltaT = simTime / N
    return (N, deltaX, deltaT, simTime, timeRoundtrip)


def fun_f(x, t, w):
    """
    Test function "f".

    Input: 
        x (float) -> position 
        t (float) -> time
    Returns:
        f (float) -> test function at "x" and "t"
    """
    c = FC.c
    k = w / c
    f = (x + c*t) * np.exp(1j * (k*x - w*t) )
    return f

def test_initialE(M=None):
    if(M==None): M=500
    dist = None
    loc = 0.5
    scale = 0.05
    E = MAX.initial_E(M, dist, loc, scale)
    x = np.linspace(0,1,M+1)

    xlabel='$x$'
    label = r'$\mathrm{Re} \left( E \right)$'
    text = 'Intervals: %d'%M
    labels = {'xlabel':xlabel, 'label':label, 'text':text}
    MAX.plot(x, np.real(E), **labels)

def test_initial_f_test(M=None):
    length = 2.0e-4 # m
    f = 25e12  # s^(-1) 
    if(M==None): M=500

    w = 2*np.pi*f
    x = np.linspace(0,1,M+1) * length
    f = MAX.initial_f_test(M, length=length, w=w)

    xlabel='$x$'
    ylabel = r'$\mathrm{Re} \left( f \right)$'
    text = 'Intervals: %d'%M
    labels = {'xlabel':xlabel, 'ylabel':ylabel, 'text':text}
    MAX.plot(x, np.real(f), **labels)


def test_Dt_f(j=0, M=None):
    """
    Creates "axes" object by plotting the time derivative of test function "f" 
    along all "M+1" gridpoints over the length at time step j (j = 0...N).
    """
    length = 2.0e-4 # m
    roundtrips = 100
    f = 25e12       # s^(-1) 
    if(M==None): M = 500

    N, deltaX, deltaT, simTime, timeRoundtrip = times(length, roundtrips, M)


    #Plot time derivative over full length at time t
    w = 2*np.pi*f
    x = np.linspace(0,1,M+1) * length
    Dt_f = MAX.timeDerivative_f_test(j, M, N, simTime, length, w)

    xlabel='$x$'
    ylabel = r'Re$\left( \partial_{t} f \right)$'
    label='Time step J = %d'%(j,)
    text = 'Intervals: %d'%M
    labels = {'xlabel':xlabel, 'ylabel':ylabel, 'label':label, 'text':text}
    MAX.plot( x, np.real(Dt_f), **labels, loc='lower left' )


    #In order to check that "MAX.timeDerivative_f" works for all time steps "J":
    #Superimpose the time derivative onto "f" as a function of "t"  at x=0
    #for "newRoundTrips" trips
    newRoundTrips = 1.0
    newN = times(length, newRoundTrips, M)[0]
#    t = np.linspace(0,newRoundTrips*timeRoundtrip ,M+1)
#    newSimTime = t[-1]
    t = np.linspace(0,newRoundTrips*timeRoundtrip ,newN+1)
    newSimTime = t[-1]
    test_f  = fun_f(x=0, t=t, w=w)
    exact_Dt = MAX.timeDerivative_f_test(j=np.arange(newN+1), M=0, N=newN, simTime=newSimTime, 
                               length=length, w=w)

    #Approximation of function "f(t)" obtained from its exact time derivative
    deltaT = t[1] - t[0]
    approx_f = np.cumsum(exact_Dt * deltaT) 
    #accomodate the values in the right positions
    approx_f = np.roll(approx_f, 1)
    approx_f[0] = 0.0


    if(newRoundTrips==0.5): title = 'Half a round trip'
    else: title = "{} round trips".format(newRoundTrips)
    ax = MAX.plot( t, np.real(test_f), label='$f$' )
    MAX.plot( t, np.real(approx_f), label='Approx. $f$ using $\partial_{t} f$', ax=ax,
          linestyle='--', alpha=0.6 )
    MAX.plot(t, np.real(exact_Dt)/1e14,'t',label=r'$\partial_{t} f/1x10^{14}$', loc='lower left',
            title=title, ax=ax, alpha=0.6, text='Intervals: %d \nx = 0'%newN)
    plt.setp(ax.lines, linewidth=3.0)
    

def test_Dx_A(method='centralDif', M=None, deepAccuracyTest=True, points='four', getArray=False):
    def exact_Dx_E(x):
        """
        Exact space derivative for Gaussian pulse
        """
        loc = 0.5
        scale = 0.05
        Dx_E = (loc - x)/(np.sqrt(2 * np.pi) * scale**3) * np.exp(-(x-loc)**2 / (2.0*scale**2) )
        return Dx_E

    length = 1 
    if(M==None): M = 500

    deltaX = length / float(M)
    x = np.linspace(0,1,M+1)

    E = MAX.initial_E(M)
    numerical_Dx = MAX.Dx_A(E, deltaX, method, points=points)
    exact_Dx = exact_Dx_E(x)

    ax = MAX.plot(x, np.real(E), label="$E$" ) 
    MAX.plot(x, np.real(exact_Dx)/100, 'x', label="exact $\partial_{x} E /100$, ", ax=ax)
#            alpha=0.3)
    MAX.plot(x, np.real(numerical_Dx)/100,'x', label="numerical $\partial_{x} E /100$, "+method, 
            ax=ax,linestyle='--', alpha=0.6, text='Intervals: %d'%M )
    plt.setp(ax.lines, linewidth=3.0)

    if(deepAccuracyTest==True):
        #In order to verify accuracy of numerical derivative method we approximate "E":
        #Approximation of function "E" obtained from its exact derivative
        approx_E = np.cumsum(exact_Dx * deltaX) 
        approx_E0 = E[0]
        #accomodate the values in the right positions
        approx_E = np.roll(approx_E, 1) + approx_E0
        approx_E[0] = approx_E0

        #Approximation of function "E" obtained from numerical derivative
        approx2_E = np.cumsum(numerical_Dx * deltaX) 
        #accomodate the values in the right positions
        approx2_E = np.roll(approx2_E, 1) + approx_E0
        approx2_E[0] = approx_E0

        title='Precision of derivative method: '+ method
        ax = MAX.plot( x, np.real(E), label='$E$' )
        MAX.plot( x, np.real(approx_E), label='Approx. $E$ using exact $\partial_{x} E$', ax=ax,
              linestyle='--', alpha=0.6 )
        MAX.plot(x, np.real(approx2_E),'x',label=r'Approx. $E$ using numerical $\partial_{x} E$',
             title=title, ax=ax, alpha=0.5, text='Intervals: %d'%M)
        plt.setp(ax.lines, linewidth=3.0)

    if(getArray): return numerical_Dx 


def test_Dxx_A(method='centralDif', M=None, deepAccuracyTest=True, getArray=False):
    """
    Compares second order derivative computed numerically from MAX.Dxx_A and the
    exact solution for the case of a Gaussian pulse.
    """
    def exact_Dxx_A(x):
        """
        Exact space derivative for Gaussian pulse
        """
        loc = 0.5
        scale = 0.05
        Dxx_A = (1 / (np.sqrt(2 * np.pi) * scale**3) ) * ( ( (x - loc)/scale )**2 - 1 ) * \
                     np.exp(-0.5*( (x-loc)/scale )**2 )
        return Dxx_A

    def exact_Dx_E(x):
        """
        Exact space derivative for Gaussian pulse
        """
        loc = 0.5
        scale = 0.05
        Dx_E = (loc - x)/(np.sqrt(2 * np.pi) * scale**3) * np.exp(-(x-loc)**2 / (2.0*scale**2) )
        return Dx_E

    length = 1 
    if(M==None): M = 500

    deltaX = length / float(M)
    x = np.linspace(0,1,M+1)

    E = MAX.initial_E(M)
    numerical_Dxx = MAX.Dxx_A(E, deltaX, method)
    exact_Dxx = exact_Dxx_A(x)


    style = {'color':'orange'}
    labels = {'label':"exact $\partial_{x}^{2} E /100$"}
    ax = MAX.plot(x, np.real(exact_Dxx)/100, 'x', **style, **labels)

    style = {'linestyle':'--', 'alpha':0.6, 'color':'green'}
    labels = {'label':"numerical $\partial_{x}^2 E /100$, "+method, 'text':'Intervals: %d'%M}
#    MAX.plot(x, np.real(numerical_Dxx)/100,'x', label="numerical $\partial_{x}^2 E /100$, "+method, ax=ax,text='Hi',
#            **style)
    MAX.plot(x, np.real(numerical_Dxx)/100,'x', ax=ax, **labels, **style)

    plt.setp(ax.lines, linewidth=3.0)

    if(deepAccuracyTest==True):
        #In order to verify accuracy of numerical derivative method we approximate "DxE"
        exact_DxE = exact_Dx_E(x)

        #Approximation of "DxE" obtained from its exact second derivative
        approx_DxE = np.cumsum(exact_Dxx * deltaX) 
        approx_DxE0 = exact_DxE[0]
        #accomodate the values in the right positions
        approx_DxE = np.roll(approx_DxE, 1) + approx_DxE0
        approx_DxE[0] = approx_DxE0

        #Approximation of "DxE" obtained from numerical second derivative
        approx2_DxE = np.cumsum(numerical_Dxx * deltaX) 
        #accomodate the values in the right positions
        approx2_DxE = np.roll(approx2_DxE, 1) + approx_DxE0
        approx2_DxE[0] = approx_DxE0

        labels = dict(label='$\partial_{x} E$')
        ax = MAX.plot( x, np.real(exact_DxE), **labels)

        label='Approx. $\partial_{x}E$ using exact $\partial_{x}^2 E$'
        labels = {'label':label}
        style = {'alpha':0.6}
        MAX.plot( x, np.real(approx_DxE), **labels, **style, ax=ax)

        title='Precision of second derivative method: '+ method
        label='Approx. $\partial_{x}E$ using numerical $\partial_{x}^2 E$'
        xlabel='x'
        labels = {'xlabel':xlabel, 'label':label, 'title':title, 'text':'Intervals: %d'%M}
        style = {'linestyle':'--', 'alpha':0.5}
        MAX.plot(x, np.real(approx2_DxE),**labels, **style, ax=ax)

        plt.setp(ax.lines, linewidth=3.0)

    if(getArray): return numerical_Dxx 


def oneWaveUpdate_LaxWen_testCase(j=1, k=0, direction='forward'):
    """
    Calculates electric field in given "direction" at time step "j" in infinitely 
    big system using the test function "fun_f", where required parameters are 
    taken from "testParams".

    j (int) -> time step
    """
    #parameters
    M, N, length, simTime, deltaX, deltaT, w, timeRoundtrip, roundtrips = testParams()

    #Initialize for t=0
#    E = MAX.initial_E(M)
    E = MAX.initial_E(M, dist='zero')
    f = MAX.initial_f_test(M, length, w)
    Dtf = MAX.Dt_f(0, case='test')

    x = np.linspace(0, length, M+1)
    ax = MAX.plot(x, np.real(E),'x','E',label='initial E')
    for i in range(j):
        E = MAX.oneWaveUpdate_LaxWen(E, f, Dtf, deltaX, deltaT, k, direction=direction)
        step = i+1
        t = (step/float(N)) * simTime 
        f = fun_f(x, t, w)
        Dtf = MAX.Dt_f(step, case='test')
    MAX.plot(x, np.real(E), label='calculated E at j = %d'%j, text="Intervals: %d"%M, loc='lower left', ax=ax)
    plt.setp(ax.lines, linewidth=3.0)
    return (E, ax)


def compareE(j=1):
    """
    Campare the calculated E in the forward direction to the exact E at time 
    step "j" for the test case. Required parameters are taken from "testParams".

    """
    #parameters
    M, N, length, simTime, deltaX, deltaT, w, timeRoundtrip, roundtrips = testParams()

    x = np.linspace(0, length, M+1)
    t = (j/float(N)) * simTime 
    beta = w / FC.c
    exactE = x * t * np.exp(1j* (beta*x - w*t))
    approxE, ax = oneWaveUpdate_LaxWen_testCase(j)

    xlabel='x'
    label='exact E at j = %d'%j
    labels = {'xlabel':xlabel, 'label':label, 'text':'Intervals: %d'%M}
    style = {'linestyle':'--', 'alpha':0.5}
    MAX.plot(x, np.real(exactE), **labels, **style, loc='lower left', ax=ax)


def myfieldAmplitudesUpdate_testCase(j=1, r1=0.8, r2=0.8, k=0, 
                      fieldAmpFunc=None, method='Lax-Wendroff'):
    """
    Calculates electric field amplitudes at time step "j" considering  reflections
    using the test function "fun_f". Required parameters are taken from "testParams".

    Input:
        j (int) -> time step
        fieldAmpFunc -> specific 'fieldAmplitudesUpdate' function to use. 
                        If set to None, 'MAX.fieldAmplitudesUpdate_v2' is used
    """
    if(fieldAmpFunc == None):
        fieldAmpFunc=MAX.fieldAmplitudesUpdate_v2
    #parameters
    M, N, length, simTime, deltaX, deltaT, w, timeRoundtrip, roundtrips = testParams()

    #Initialize for t=0 adding two extra points beyond the simulation window
    E_left = MAX.initial_E(M+2, dist='zero')
    E_right = MAX.initial_E(M+2, dist='zero')
    f = np.zeros(M+3, dtype=complex)
    f[1:-1] = MAX.initial_f_test(M, length, w)
    Dtf = np.zeros(M+3, dtype=complex)
    Dtf[1:-1] = MAX.Dt_f(0, case='test')

    x = np.linspace(0, length, M+1)

    title='Field amplitudes using method: '+ method
    xlabel='$x$'
    label1 = r'$\mathrm{Re} \left( E_{-} \right)$ initial'
    label2 = r'$\mathrm{Re} \left( E_{+} \right)$ initial'
    label3 = r'$\mathrm{Re} \left( E_{-} \right)$ at j = ' + str(j) 
    label4 = r'$\mathrm{Re} \left( E_{+} \right)$ at j = ' + str(j)
    text = 'Intervals: %d'%M

    labels1 = {'title':title,'xlabel':xlabel, 'label':label1, 'text':text}
    labels2 = {'label':label2}
    labels3 = {'label':label3}
    labels4 = {'label':label4}

    ax = MAX.plot(x, np.real(E_left[1:-1]), **labels1 )
    MAX.plot(x, np.real(E_right[1:-1]), **labels2, ax=ax )
    for i in range(j):
        E_left, E_right = fieldAmpFunc(E_left, E_right, f, Dtf, r1, r2, deltaX, deltaT, k, 
                                                   method=method)
        step = i+1
        t = (step/float(N)) * simTime 
        f[1:-1] = fun_f(x, t, w)
        Dtf[1:-1] = MAX.Dt_f(step, case='test')
    MAX.plot(x, np.real(E_left[1:-1]), **labels3, ax=ax)
    MAX.plot(x, np.real(E_right[1:-1]), **labels4, loc='lower left', ax=ax)
    plt.setp(ax.lines, linewidth=3.0)

    #Remove points outside simulation window and finish
    return (E_left[1:-1], E_right[1:-1], ax)



def test_electricField(j=1, r1=0.8, r2=0.8, k=0, 
                fieldAmpFunc=None, method='Lax-Wendroff'):
    """
    Plot the final electric field for the test case at time step "j". 
    Required parameters are taken from "testParams".
    """
    if(fieldAmpFunc == None):
        fieldAmpFunc=MAX.fieldAmplitudesUpdate_v2
    #parameters
    M, N, length, simTime, deltaX, deltaT, w, timeRoundtrip, roundtrips = testParams()
    x = np.linspace(0, length, M+1)
    t = (j/float(N)) * simTime 

#    (E_left, E_right, ax) = fieldAmplitudesUpdate_v1_testCase(j, r1, r2, k, method)
    (E_left, E_right, ax) = myfieldAmplitudesUpdate_testCase(j, r1, r2, k, fieldAmpFunc, method)
    E = MAX.electricField( E_left, E_right, x, t, w )
    MAX.plot(x, E,"$x$","$E$", "Total electric field at time step %d"%j, text='Intervals: %d'%M)

def runTests():
    test_initialE()
    test_initial_f_test()
    test_Dt_f()
    test_Dx_A()
    test_Dxx_A()
    compareE(j=100)
    test_electricField(j=100)

