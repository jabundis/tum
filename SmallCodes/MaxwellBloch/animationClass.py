from matplotlib.animation import FuncAnimation
import numpy as np

class Animation(object):
    def __init__(self, ax, updateData, YaxDimFixed=True):
        """
        Definition:
	    ax (Axes instance) -> Must contain the Line2D instances to be updated 
                                  at each frame during animation.

            updateData (function) -> Function that takes as input an integer (frame 
                                    number) and returns the data for the Line2D objects.

                                    It must return a list of tuples, where each 
                                    tuple contains two real ndarrays: x, y
                                    i. e. [(x1, y1), (x2,y2)]
                                    Note: number of elements in this list must equal
                                          the number of Line2D instances in 'ax'.

            YaxDimFixed (bool) -> Defines whether Y axis limits should remain as 
                                  initially set in "ax" or be updated dynamically.
        """
        self.ax = ax
        self.updateData = updateData
        self.fig = ax.get_figure()
        self.lines = ax.lines   #List of lines
        self.YaxDimFixed = YaxDimFixed
#        self.anim = None

    def setYdim(self, y):
        ymin, ymax = self.ax.get_ylim()
        arrayMax = np.max(y)
        arrayMin = np.min(y)
        interv = ymax - ymin
        if(arrayMax > ymax):
            if(arrayMin < ymin):
                self.ax.set_ylim(arrayMin - 0.2*interv, arrayMax + 0.2*interv)
            else: 
                self.ax.set_ylim(ymin, arrayMax + 0.2*(ymax-ymin))
            self.ax.figure.canvas.draw()
        elif(arrayMin < ymin):
            self.ax.set_ylim(arrayMin - 0.2*interv, ymax)
            self.ax.figure.canvas.draw()

    def init(self):
        for line in self.lines:
            line.set_data([], [])
        return self.lines

    def __call__(self, i):
        dataList = self.updateData(i)
        for i, data in enumerate(dataList):
            line = self.lines[i]
            x = data[0]; y = data[1]
            if(not self.YaxDimFixed): self.setYdim(y)
            line.set_data( x, y )
        return self.lines

    def runAnimation(self, frames, interval=50, blit=True):
        global anim
        anim = FuncAnimation(self.fig, self, frames=frames, init_func=self.init,  
                             interval=interval, blit=blit)

