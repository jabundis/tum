import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
I1, MIR1 = np.loadtxt('MIR1.dat', unpack=True)
I2, MIR2 = np.loadtxt('MIR2.dat', unpack=True)
ITHz, THz = np.loadtxt('THz.dat', unpack=True)
I_interp, V = np.loadtxt('Current_Voltage.dat', unpack=True)

f_MIR1 = interpolate.interp1d(I1, MIR1)
f_MIR2 = interpolate.interp1d(I2, MIR2)
f_THz = interpolate.interp1d(ITHz, THz,fill_value="extrapolate")
#Powers as a function of V
MIR1_V = f_MIR1(I_interp)
MIR2_V = f_MIR2(I_interp)
THz_V = f_THz(I_interp)

plt.plot(V*2, MIR1_V, V*2, MIR2_V, V*2, THz_V*19)
