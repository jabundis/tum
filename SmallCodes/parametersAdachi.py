import numpy as np
import matplotlib.pyplot as plt

#Materials:
materials = ('In','Ga','Al', 'As','P','Sb')

#Format 'Values' index: { material: (A , B), ... }
strengthParam = {'Indices':{'A':0, 'B':1},
                 'Values':{"AlP":(18.39,-0.74), "InAs":(5.14,10.15), "GaAs":(6.30,9.40), 
                           "AlAs":(25.30,-0.80),"InP":(8.40, 6.60), "GaP":(14.71,4.38),
                           "AlSb":(59.68,-9.53)}
                }
#Format 'Values' index: { material: (E0, Delta0, EgX, EgL), ... } in eV
energies ={'Indices':{'E0':0, 'Delta0':1, 'EgX':2, 'EgL':3},
           'Values':{"AlP":(3.58,0.10,2.48,3.30), "AlAs":(2.95,0.28,2.16,2.36),
                     "GaAs":(1.42,0.34,1.91,1.73),"InP":(1.35,0.10,2.21,2.05),
                     "InAs":(0.36,0.40,1.37,1.07),"GaP":(2.74,0.10,2.26,2.63),
                     "AlSb":(2.30,0.72,1.61,1.21)}, 
           'BowingParam':{"InGaAs":(0.6, 0.20, 1.4, 0.72), "AlGaAs":(0.37,0.07,0.245, 0.055),
                          "AlInAs":(0.70, 0.15, 0, 0)}
           }

def strengthParamAlloys(alloy, x=None):
    """
    Returns the strength parameters for a binary or ternary alloy entered as "alloy".
    If a ternary is specified, "x" represents the percent of the first binary. "x" is
    not required (ignored) for the case of a binary.

    Input:
        alloy (string) -> binary or alloy
        x (float in [0,1]) -> percent of first binary in case of ternary.
    Returns:
        (A, B) -> tuple with parameters A and B.
    """
    global materials, strengthParam
    elements = findElements(alloy, materials)
    if(len(elements)==2):  #A binary is used
        aBinary = binary(elements[0], elements[1], strengthParam)
        return strengthParam['Values'][aBinary]
    elif(len(elements)==3):  
        if(x==None): raise Exception('Please enter value for x (float in range [0,1])')
        binaries = binariesFromTernary(elements, strengthParam)
        contents = (x, 1-x) 
        A_B = 0
        for i, aBinary in enumerate(binaries):
            A_B += contents[i] * np.array( strengthParam['Values'][aBinary] )
        A = A_B[0]; B = A_B[1]
        return (A,B)
    else:
        raise Exception("alloy should be either a binary or ternary")

def energyParamAlloys(alloy, x=None, paramList=['E0', 'Delta0']):
    """
    Returns the energy parameters in "paramList" (E0 and Delta0 by default) for a 
    binary or ternary alloy entered as "alloy". If a ternary is specified, "x" 
    represents the percent of the first binary. "x" is not required (ignored) for 
    the case of a binary.

    Input:
        alloy (string) -> binary or alloy
        x (float in [0,1]) -> percent of first binary in case of ternary.
                              Ignored in case of binary.
        paramList (List) -> parameters to be returned
    Returns:
        tuple with Energy parameters (by default (E0, Delta0))
    """
    global materials, energies
    elements = findElements(alloy, materials)
    if(len(elements)==2):  #A binary is used
        aBinary = binary(elements[0], elements[1], energies)
        indList = [energies['Indices'][param] for param in paramList]
        return tuple([energies['Values'][aBinary][ind] for ind in indList])
    elif(len(elements)==3):  
        if(x==None): raise Exception('Please enter value for x (float in range [0,1])')
        bandgaps = tuple([ parabolicApprox(alloy, x, param) for param in paramList])
        return bandgaps
    else:
        raise Exception("alloy should be either a binary or ternary")

def parabolicApprox(alloy, x, param='E0', dictionary=energies, materials=materials):
    """
    Finds parabolic approximation of "param" in "dictionary" for "alloy". Alloy
    is a ternary given as a string: 'ABC'. It's assumed AxB(1-x)C. 
    Considers bowing parameter.
    """
    elements = findElements(alloy, materials)
    assert len(elements)==3, "alloy should be ternary"
    binaries = binariesFromTernary(elements, dictionary)
    ind = dictionary['Indices'][param] #Index of sought parameter
    alloy = ''
    for elem in elements: 
        alloy += elem        #Elements in Alloy are capitalized
    if(alloy not in dictionary['BowingParam']):
        alloy = elements[1] + elements[0] + elements[2]
        if(alloy not in dictionary['BowingParam']):
            raise Exception("Bowing Parameters not found in dictionary for {}".format(alloy))
    approximation = ( x*dictionary['Values'][binaries[0]][ind] +  
                      (1-x)*dictionary['Values'][binaries[1]][ind] -   
                      x*(1-x)*dictionary['BowingParam'][alloy][ind] )
    return approximation

def findElements(alloy, materials):
    """
    Finds atoms used in alloy that are contained in the list "materials".
    The elements are extracted from left to right according to "alloy"
    and are capitalized. If no atoms are found returns an Exception.
    
    Input:
        alloy (string) -> alloy
        materials (list or tuple) -> Set of possible atoms

    Output:
        elements (list) -> Atoms that comprise alloy
    """
    materials = [materials[i].lower() for i in range(len(materials))]
    elements = []
    ind = 0
    n = len(alloy)
    while(ind < n):
        if( alloy[ind:ind+2].lower() in materials ):
            elem = alloy[ind:ind+2].capitalize()
            elements.append(elem)
            ind += 2
        elif( alloy[ind:ind+1].lower() in materials ):
            elements.append( alloy[ind:ind+1].capitalize() )
            ind += 1
        else:
            raise Exception('At least one material is not found in materials tuple. Check your Alloy')
    return elements

def binary(elem1, elem2,dictionary):
    element = ""
    if( elem1+elem2 in dictionary['Values'] ): element = elem1+elem2
    elif( elem2+elem1 in dictionary['Values'] ): element = elem2+elem1
    return element

def binariesFromTernary(elements, dictionary):
    """
    Returns list with binaries contained in "dictionary". 
    "elements" is a list containing the three atoms used in the ternary.
    """
    binaries = []; 
    for i in range(2):
        if( len( binary(elements[i],elements[2],dictionary) )!=0 ): 
            binaries.append( binary(elements[i],elements[2],dictionary) )
        else:
            raise Exception("Binary {} was not found in dictionary".format(elements[i]+elements[2]))
    return binaries

def findParameters(alloy, x=None):
    """
    Returns tuple with Adachi's interband transition parameters for "alloy". "alloy"
    is a binary or ternary: If a ternary,it is given as: 'ABC' which is assumed to be 
    'AxB(1-x)C'. In case of a binary "x" is not required (ignored).

    Input:
        alloy (string) -> binary or ternary alloy
        x (float in closed interval [0,1]) -> Percent of first binary in "alloy".
                                              Ignored in case of a binary.

    Returns:
        tuple -> (A, B, E0, Delta0)
    """
    strengths = strengthParamAlloys(alloy, x)
    bandgaps = energyParamAlloys(alloy, x)
    return strengths + bandgaps

def parametersInGaAs(x=0.53):
    """
    Returns tuple with interband transition parameters for In(x)Ga(1-x)As
    that enter Adachi's formula (x=0.53 by default).

    Returns:
        tuple -> (A, B, E0, Delta0)
    """
#    strengths = strengthParamAlloys("InGaAs", x)
#    bandgaps = tuple([ parabolicApprox("InGaAs", x, param) for param in ["E0", "Delta0"] ])
#    return strengths + bandgaps
    return findParameters('InGaAs', x)

def refIndex_transparentRegion(alloy, x, f):
    """
    x is ignored (can be set to None) when alloy is a binary.
    """
    def func(x):
        return x**-2 * (2-np.sqrt(1+x)-np.sqrt(1-x))
    from fundamentalConstants import hev
    f = np.array(f)
    param =  findParameters(alloy, x)
    w = 2*np.pi*f; E=hev*w
    A, B, E0, Delta0 = param
    x0 = E / E0; xs0 = E / (E0 + Delta0) 
    if(np.any(x0>1)): 
        indHighE = np.where(x0>1)[0][0] #Index of first Freq. with exceeding energy
        highF = f[indHighE]
        highE = E[indHighE]
        raise Exception("Freq. %.4e has energy %seV (larger than bandgap %seV)" % (highF, highE,E0))
    nSquared = A * (func(x0) + 0.5*func(xs0)*(E0/(E0+Delta0))**(1.5)) + B
    return np.sqrt(nSquared)

def makePlotRefIndex(alloy, x, E, label):
    """
    E (array-like) -> Energies in eV
    """
    from fundamentalConstants import hev
    E = np.array(E)
    w =  E / hev
    f = w / (2 * np.pi)
    refIndex = refIndex_transparentRegion(alloy, x, f)
    plt.plot(E, refIndex, label=label)
    plt.xlabel(r'$\hbar \omega$ (eV)', fontsize=14)
    plt.ylabel(r'$n$', fontsize=14)
    plt.legend()

def makeAllPlots():
    import matplotlib
    with matplotlib.rc_context(rc={'lines.linewidth':2}):
        fig = plt.figure(figsize=(6,8))
        ax = fig.add_subplot(1,1,1)
        E = np.linspace(0.2,2.9)
        makePlotRefIndex('AlAs', None, E, 'AlAs')
        E = np.linspace(0.2,3.57)
        makePlotRefIndex('AlP', None, E, 'AlP')
        E = np.linspace(0.2,2.7)
        makePlotRefIndex('GaP', None, E, 'GaP')
        E = np.linspace(0.2,2.29)
        makePlotRefIndex('AlSb', None, E, 'AlSb')
        ax.axis([0,4,2.5,6])
#        ax.xaxis.set_label_text(r'$\hbar \omega$ (eV)', fontsize=16)
#        ax.yaxis.set_label_text(r'$n$',fontsize=16)
#        plt.legend()
