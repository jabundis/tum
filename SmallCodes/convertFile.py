import numpy as np
import freqAndE as FE
import sys

def getFiles():
    #No argument introduced
    if(len(sys.argv)==1):
        File = input("Enter Input File: ")
        return (File,)
    #Only inputfile was provided
    elif(len(sys.argv)==2):
        return (sys.argv[1],) 
    #Both input and output files were provided
    elif(len(sys.argv)==3):
        File = sys.argv[1] ; outputFile = sys.argv[2]
        return (File, outputFile) 
    else:
        raise Exception("Must introduce at most two parameters")

def changeWavelenForFreq(File, firstColumn='wavelen', outputFile='outFile.dat'):
    """
    Converts first column in "File"  into frequencies [Hz]. The file must 
    contain three columns. The first one must contain either wavelengths [micron]
    or energies [eV]. The result is stored in a file with name "outputFile".

    Structure for "File":
    wavelength[micron] (or energies[eV])    a    b
    
    Input:
        File (string) -> File to read
        firstColumn (string) --> set equal to 'wavelen' for wavelengths [micron]
                                 or equal to 'energy' for energies [eV]
        outputFile (string) -> Output file name

    Returns:
        None. It creates a new file "outputFile" with structure:
        frequency[Hz]    a    b
    """
    firstCol, a, b = np.loadtxt(File, unpack=True)
    firstColumn = firstColumn.lower()
    if(firstColumn=='wavelen'): f = FE.wavelenToFreq(firstCol * 1e-6)
    elif(firstColumn=='energy'): f = FE.energyToFreq(firstCol) 
    else: raise Exception('firstColumn must be either "wavelen" or "energy"') 
    mat = np.zeros((len(f),3))
    mat[:,0] = f; mat[:,1] = a; mat[:,2] = b
    ind = np.argsort(f)
    mat = mat[ind]
    np.savetxt(outputFile, mat, ['%.18e','%.3f','%.5f'], delimiter='    ')


if(__name__=='__main__'):
    """
    When script is run directly from command line it is assumed to
    have a file with first column being wavelengths in microns. The 
    first argument takes the input filename and, optionally, a second
    argument takes the output filename ('outFile.dat' by default).
    Ex.
        python3 convertFile.py inputFile outputFile 
    """
    enteredFiles = getFiles()
    File = enteredFiles[0]
    outputFile = 'outFile.dat'
    if( len(enteredFiles)==2 ):
        outputFile = enteredFiles[1]
    changeWavelenForFreq(File, outputFile=outputFile) 



