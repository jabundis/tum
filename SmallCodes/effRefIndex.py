from doping import L_Belkin

import numpy as np

def effectiveInd(nBarrier, nWell, thicknesses):
    """
    Calculates the effective refractive index.

    Input:
        nBarrier (float or int) --> Ref. index of barrrier material
        nWell (float or int) --> Ref. index of well material
        thicknesses --> array-like object with layer thicknesses starting with
                 barrier and alternating with well as in: [barrier1, well1,
                 barrier2, well2, ...]
    Output:
        effIndex (float) --> Effective refractive index
    """
    n = [nBarrier, nWell]
    Lp = sum(thicknesses)
    prod = 0
    for i, L in enumerate(thicknesses):
        prod += n[i%2] * L
    effIndex = prod /Lp
    return effIndex

nBarrier = 3.146; nWell = 3.322;
thicknesses = L_Belkin
