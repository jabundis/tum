from fundamentalConstants import e, me

#resistivities: [rho] = ohm * cm (taken from: Semiconductors: Data Handbook 3rd Ed., Otfried Madelung)
rho_dict = {'GaAs':(2.38e-9)**-1, 'InAs':1/50.0}

def tau(ne, rho, me, e):
    """
    Input:
        ne (float) -> electron density [cm-3]
        rho (float) -> resistivity [ohm * cm]
        me -> electron mass [kg]
        e -> electron charge [C]

    Returns:
        float -> Drude relaxation time [picoseconds]
                 (mean time between collisions)
    """
    #SI units
    ne = ne*1e6
    rho = rho/100.0 

    return me/(rho*ne*e**2) * 1e12

