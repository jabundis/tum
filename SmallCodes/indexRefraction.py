import numpy as np
import fundamentalConstants as FC
import sys
"""
Introduce as a command argument the frequency in hertz at which 
to calculate the refractive index
"""
if(len(sys.argv)==1):
    f = 30e12   #[hertz]
elif(len(sys.argv)==2):
    f = float(sys.argv[1])
else:
    raise Exception("Must introduce only one parameter")

#Frequency
#f = 30e12   #[hertz]
#Aluminum Contents
x = 0.48  #[Percent]

E0 = 2.21*x + 2.02  #[eV]
Ed = 13.2*x + 20.9  #[eV]
E = FC.hev * (2*np.pi * f)
n = np.sqrt(1 + E0*Ed/(E0**2 - E**2))

print("frequency: {:.3e}".format(f))
print("n = {:.3f}".format(n))
