import numpy as np
import matplotlib.pyplot as plt 
from scipy import interpolate
I1, MIR1 = np.loadtxt('digitizedData/MIR1.dat', unpack=True)
I2, MIR2 = np.loadtxt('digitizedData/MIR2.dat', unpack=True)
ITHz, THz = np.loadtxt('digitizedData/THz.dat', unpack=True)
I_interp, V = np.loadtxt('digitizedData/Current_Voltage.dat', unpack=True)

f_MIR1 = interpolate.interp1d(I1, MIR1)
f_MIR2 = interpolate.interp1d(I2, MIR2)
f_THz = interpolate.interp1d(ITHz, THz,fill_value="extrapolate")
#Powers as a function of V
MIR1_V = f_MIR1(I_interp)
MIR2_V = f_MIR2(I_interp)
THz_V = f_THz(I_interp)

filename = 'powers.txt'
E,Mod1Out, Mod2Out, ModTHz, Current = np.loadtxt(filename,usecols=(0,2,4,7,5), unpack=True)
ModTHz = abs(ModTHz) * 25
ModTHz[0:7] = 0
ETHz = list(E)
ETHz.insert(8,41.2); ModTHz = list(ModTHz); ModTHz.insert(7,3); ModTHz[-1]=0;
del ModTHz[-1]
del ETHz[-1]

ModTHz = np.array(ModTHz)

example = 2
tickSize = 35
axisSize = 32
marker30THz = 10
marker28THz = 12
markerTHz = 10

#---------------------------------------------------------------------------------------------
#Example1
if(example==1):
    line1, = plt.plot(E, Mod1Out, 'b-*', linewidth=2, label='30.906THz')
    line2, = plt.plot(E, Mod2Out, 'r-o', linewidth=2, label='28.018THz')
    plt.xlabel("E [kV/cm]")
    plt.ylabel("P [mW]")

    ax1 = plt.gca()
    ax2 = ax1.twinx()
    ax2.plot(V*2, THz_V, 'g--')
    for tl in ax2.get_yticklabels():
            tl.set_color('g')
    ax2.set_ylabel(r'THz Power [$\mu$W]', color='g')
    ax2.set_ylim(top=40)
    ax2.set_ylim(bottom=-1.8)

    ax1.plot(2*V, MIR1_V, 'r--')
    ax1.plot(2*V, MIR2_V, 'b--')
    line3, = ax2.plot(ETHz, ModTHz, 'g-.',label='2.888THz')
    ax1.legend(handles=[line1, line2, line3])

#---------------------------------------------------------------------------------------------
#Example2
elif(example==2):
    ETHz = np.array(ETHz); 
    ind = np.where(E<=44)
    ind2 = np.where(2*V <=45)
    ind3 = np.where(ETHz<=44)

    fig = plt.figure(figsize=(8,6))
    line1, = plt.plot(E[ind], Mod1Out[ind], 'b-*', linewidth=2, label='30.906THz', markersize=marker30THz)
    line2, = plt.plot(E[ind], Mod2Out[ind], 'r-o', linewidth=2, label='28.018THz', markersize=marker28THz)
    plt.xlabel("Bias [kV/cm]")
    plt.ylabel("MIR power [mW]")

    ax1 = plt.gca()
    ax2 = ax1.twinx()
    ax2.plot(V[ind2]*2, THz_V[ind2], 'g--')
    for tl in ax2.get_yticklabels():
            tl.set_color('g')
    ax2.set_ylabel(r'THz Power [$\mu$W]', color='g')
    ax2.set_ylim(top=40)
    ax2.set_ylim(bottom=-1.8)

    ax1.plot(2*V[ind2], MIR1_V[ind2], 'r--')
    ax1.plot(2*V[ind2], MIR2_V[ind2], 'b--')
    line3, = ax2.plot(ETHz[ind3], ModTHz[ind3], 'g-^',label='2.888THz', markersize=markerTHz)
    ax1.legend(handles=[line1, line2, line3], fontsize=18)

    ax1.xaxis.set_ticks(range(32,45,4))
    ax1.tick_params(labelsize=tickSize)
    ax2.tick_params(labelsize=tickSize)

    ax1.xaxis.label.set_size(axisSize)
    ax1.yaxis.label.set_size(axisSize)
    ax2.yaxis.label.set_size(axisSize)

    fig.canvas.draw()

#    fig.savefig('comparisonPower.pdf',bbox_inches='tight')
