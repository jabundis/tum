import fundamentalConstants as FC
import numpy as np

def waveInfo(waveLength, n=1):
    """
    Returns and prints the frequency and energy of a wave with 
    wavelength "waveLength" and refractive index "n". 

    Input:
        waveLength (float or array-like) [m]
        n (float or int)

    Returns:
        (freq, energy)   where:
            freq --> Frequency [s^-1]
            energy [eV]
    """
    waveLength = np.array(waveLength)
    freq = (FC.c/n) / waveLength
    w = 2*np.pi * freq
    energy = FC.hev * w
    print("Freq: {:.3e}s^-1, Energy: {:.4f}eV".format(freq, energy))
    return (freq, energy)

def wavelenToFreq(waveLength, n=1):
    """
    Returns frequencies in Hz for waves of wavelength "waveLength" 
    and refractive index "n". Vacuum is taken by default (n=1).

    Input:
        waveLength (float or array-like) [m]
        n

    Returns:
        freq (float or np.ndarray) [Hz]
    """
    waveLength = np.array(waveLength)
    freq = (FC.c/n) / waveLength
    return freq 

def wavenumToFreq(wavenum, n=1):
    """
    Returns the linear frequencies in Hz for wavenumbers "wavenum" in cm^-1.
    Notice that by default vacuum wavenumbers are considered.
    Definition: wavenum = 1 / lambda is used (no 2*pi factor).
    Input:
        wavenum (float or array-like) --> wavenumbers [cm-1] 
        n --> refractive index
    Returns:
        freq (float or np.ndarray) --> linear frequencies [Hz]
    """
    wavenum = np.array(wavenum)
    waveLength = (1 / wavenum) *1e-2
    return wavelenToFreq(waveLength, n)

def energyToFreq(energy):
    """
    Returns the frequencies in Hz for energies "energy" in eV.
    Input:
        energy (float or array-like) [eV]
    Returns:
        freq --> linear frequencies [Hz]
    """
    energy = np.array(energy)
    w = energy / FC.hev
    f = w / (2*np.pi)
    return f

def freqToEnergy(freq):
    """
    Returns energy in eV for frequencies "freq" in hertz
    Input:
        freq (float or array-like)--> linear frequencies [hertz]

    Returns:
        E (float or np.ndarray) --> Energy [eV]
    """
    freq = np.array(freq)
    w = 2*np.pi*freq
    E = FC.hev * w
    return E

def freqToWavelength(freq, n=1):
    """
    Returns wavelength in microns for frequencies "freq" in hertz
    in a medium of refractive index "n" (vacuum by default)

    Input:
        freq (float or array-like) --> linear frequencies [hertz]

    Returns:
        wavelength (float or np.ndarray) --> Wavelength [microns]
    """
    freq = np.array(freq)
    waveLength = (FC.c / n) / freq  #lambda in m
    return waveLength * 1e6
