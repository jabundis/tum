import numpy as np
import fundamentalConstants as FC
import sys
"""
Calculates the refractive index for In(0.52)Al(0.48)As
as a function of frequency.

If run as a script, introduce as a command argument the 
frequency in hertz at which to calculate the refractive index. 
If not introduced f=30Thz is used by default
"""
def getFreq():
    #Default frequency
    if(len(sys.argv)==1):
        return 30e12   #[hertz]
    #Introduced frequency
    elif(len(sys.argv)==2):
        return float(sys.argv[1]) #[hertz]
    else:
        raise Exception("Must introduce only one parameter")

def getRefractiveIndex(freq): 
    """
    Returns refractive index for a given frequency in "hertz"
    """
    #retrieve vacuum wavelength
    Energy = FC.hev * (2*np.pi * freq) #[eV]
    wavelength = 1.2398 / Energy #[microns]

    #model constants
    A = 8.650 
    B = 1.239 
    C = 0.7308  #[microns] 

    n = np.sqrt(A + B*wavelength**2 / (wavelength**2 - C**2))
    return n

def allRefractiveIndices(freqList):
    """
    Returns a numpy array with refractive indices for each 
    frequency in the array-like object "freqList"
    """
    refIndexArray = np.zeros(len(freqList))
    for i, freq in enumerate(freqList):
        refIndexArray[i] = getRefractiveIndex(freq)
    return refIndexArray

if (__name__=="__main__"):
    freq = getFreq()
    n = getRefractiveIndex(freq)
    print("frequency: {:.3e}".format(freq))
    print("n = {:.3f}".format(n))
